#!/bin/bash
set -x
if [ -f ~/.ssh/devicePubKey ] && [ "$8" != "new" ]
then
echo ''
else
   if [ -f ~/.ssh/id_rsa ]
   then
      ssh-keygen -y -f ~/.ssh/id_rsa >> ~/.ssh/id_rsa.pub
   else
      ssh-keygen -t rsa -f ~/.ssh/id_rsa -N ''
   fi
   mv -f ~/.ssh/id_rsa.pub  ~/.ssh/devicePubKey
   sshpass -p secret scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
      -P $6 ~/.ssh/devicePubKey $5@$2:~/devicePubKey
   sshpass -p secret ssh -p $6 -o ExitOnForwardFailure=yes \
      -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
      $5@$2 "mkdir -p ~/.ssh; \
      sh -c 'cat ~/devicePubKey >> ~/.ssh/authorized_keys' \
      ;rm ~/devicePubKey"
fi
if [ -f ~/.ssh/serverPubKey ] && [ "$8" != "new" ]
then
   echo ''
else
   #che workspaces default password is secret
   eval "cat serverScriptRSA.sh | ssh -p $6 -o ExitOnForwardFailure=yes \
   -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
   $5@$2 '/bin/bash -s' "
   scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
   -P $6 $1@$2:~/serverPubKey ~/.ssh/serverPubKey
   cat ~/.ssh/serverPubKey >> ~/.ssh/authorized_keys
fi
while true; do \
ssh -p $6 -R $7:localhost:22 \
-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
-o ExitOnForwardFailure=yes \
$1@$2 \
"sudo mkdir -p $4; sudo sshfs -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
-p $7 -o allow_other $1@localhost:$3 $4; \
{ while true; do echo echo ping; sleep 10; done }" \
 >/dev/null
sleep 10
done
