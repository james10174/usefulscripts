#General Discription#

* Create private key only if you have not done so already on device
* Copy public key from the following. You will need to add this to your server or cloud server.
* If it's your own cloud server add it to ~/.ssh/authorized_keys.
* HOST needs sshfs installed. On ubuntu: apt-get install sshfs
* CLIENT needs scp and sshpass. Not needed if you manually create ssh keypair but need to run script.

###On CLIENT###

* --Generate private/public key without passphrase 
* ssh-keygen -t rsa
* --Copy output from the following
* cat ~/.ssh/id_rsa.pub

###On HOST###

* echo "<paste id_rsa.pub from above>" >> ~/.ssh/authorized_keys

###Howto Run###

* ./sshServer.sh <server user> <server ipaddress> <device folder to share/mount> <server mount folder> <host user> <server ssh port> <server port forwarded>

###Example###

* ./sshServer.sh root <server ipaddress> / /mnt/remoteDevice/ user <server ssh port> 2000 &
