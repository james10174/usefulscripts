#!/bin/bash
set -x
if [ -f /usr/bin/sshfs ]; then
   echo 'sshfs exists'
else
   sudo apt-get update
   sudo apt-get install -y sshfs
fi
if [ -f ~/.ssh/id_rsa ]; then
   ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
else
   ssh-keygen -t rsa -f ~/.ssh/id_rsa -N '' 
fi
mv ~/.ssh/id_rsa.pub  ~/.ssh/serverPubKey
if [ -f /usr/bin/sshfs ]; then
   echo ''
else
   sudo apt-get update
   sudo apt-get install -y sshfs
fi
